# fish-le-git

A fish plugin for displaying all Git repositories within a directory and interacting with it.

## Installation

**Using** [Fisher](https://github.com/jorgebucaran/fisher)

`fisher add gitlab.com/Braeuer/fish-le-git`

**Using** [Oh My Fish](https://github.com/oh-my-fish/oh-my-fish)

`omf install gitlab.com/Braeuer/fish-le-git`

## Usage

Simply run `legit` in your fish shell and it will search for all Git repositories within your `home` directory.

## Customization

The following arguments may be passed when calling `legit` to customize its behaviour.

| Short / Long | Default | Description |
|--------------|---------|-------------|
| `-d` / `--dir <path>`| `~` | Specify the directory in which to search for Git repositories (directories starting with `.` will not be followed)

## Interactive Usage

This section describes all actions available when runing `legit`.

### Git Repository Overview

| Action  | Description |
|---------|-------------|
|  `q`    | Quit `fish-le-git`
|  `1-x`  | Choose a repository from the list to change into repository mode. The valid numbers depend on the repositories found.

### Git Repository Mode

| Action  | Description |
|---------|-------------|
|   `a`   | Add changes to the stage in order to be committed (= `git add <files|directories>`)
|   `b`   | Switch into branch mode
|   `c`   | Commit changes on current branch (= `git commit`)
|   `e`   | Execute an arbitrary command within the current repositories directory
|   `h`   | Print all actions available in this view
|   `p`   | Push all committed changes to remote branch with same name as the local branch (= `git push origin <local-branch-name>`)
|   `q`   | Quit the interactive repository view
|   `s`   | Show a summary of the status of the current branch (= `git status`)
|   `u`   | Pull changes from remote branch with same name as the local branch (= `git pull origin <local-branch-name>`)

**Warning:**<br>
The `e` action changes into the current repository's directory and lets you execute an arbitrary command.<br>
After the command has been executed it changes back to the directory where `legit` has been executed<br>
=> If you exit `fish-le-git` in this mode (e.g. with `ctrl+c`) before the directory could be changed back you<br>
   will end up in the repositoriy's directory.

### Branch Mode

| Action  | Description |
|---------|-------------|
|   `c`   | Create and switch to a new branch in the current Git repository (= `git checkout -b <branch_name>`)
|   `d`   | Delete a branch in the current Git repository (= `git branch -d <branch_name>`)
|   `h`   | Print available options of branch mode
|   `l`   | List all local branches of the current repository (= `git branch`)
|   `n`   | Create a new branch in the current Git repository (= `git branch <branch_name>`)
|   `q`   | Quit branch mode
|   `s`   | Switch to a branch (= `git checkout <branch_name>`)

# Special Thanks to

* [Benedikt Vollmerhaus](https://gitlab.com/BVollmerhaus) - For testing and creative input

# License

`fish-le-git` is licensed under the MIT license. See [LICENSE](https://gitlab.com/Braeuer/fish-le-git/blob/master/LICENSE) for more information.
