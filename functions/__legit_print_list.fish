#-------------------------------------------------------------------------------
#
# Description : Function for printing a newline separated list in
#               format: "<index>: <list-element>\n"
#
# Author      : Matthias Bräuer <matthias@braeuer.dev>
# Date        : 2019-10-26
# License     : MIT
#
#-------------------------------------------------------------------------------

function __legit_print_list
    argparse --name=__legit_print_list 'c/color=' 'n/nocount' -- $argv
    or return

    set -q _flag_match; or set -l _flag_match ""
    set -q _flag_color; or set -l _flag_color info

    set -l count 0

    for elem in $argv
        set count (math $count + 1)

        if test -z $_flag_nocount
            printf "\t%2d: %s%s%s\n" $count \
                    (__legit_set_color $_flag_color) $elem (__legit_set_color)
        else
            printf "\t%s%s%s\n" \
                    (__legit_set_color $_flag_color) $elem (__legit_set_color)
        end
    end
end
