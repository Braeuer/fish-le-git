#-------------------------------------------------------------------------------
#
# Description : A fish shell function for interacting with a Git repository
#               and executing arbitrary commands within that repository.
#
# Author      : Matthias Bräuer <matthias@braeuer.dev>
# Date        : 2019-10-26
# License     : MIT
#
#-------------------------------------------------------------------------------

function __legit_repo_interact -a "repo_path"
    set -l curr_pwd (pwd)

    _legit_repo_interact_print_current $repo_path

    while true
        read -n 1 --prompt "_legit_repo_interact_print_prompt_help" -l action

        switch $action
            case a
                _legit_repo_interact_add $repo_path
            case c
                _legit_repo_interact_commit $repo_path
            case b
                clear
                __legit_branch_interact $repo_path
                clear
                _legit_repo_interact_print_current $repo_path
            case e
                cd $repo_path
                read --shell --prompt 'printf ":: [Command] %s> " (prompt_pwd)' \
                                       -l command
                printf "\n"
                eval $command
                cd $curr_pwd
            case h
                _legit_repo_interact_print_help
            case p
                set -l branch_name (command git -C $repo_path rev-parse --abbrev-ref HEAD)

                if test $branch_name = HEAD
                    __legit_print_list -c error -n \
                    "\nError: Cannot push changes. Seems like branch is in detached mode.\n"
                else
                    _legit_repo_interact_push $repo_path $branch_name
                end
            case q
                clear
                cd $curr_pwd
                return
            case s
                __legit_show_status $repo_path
            case u
                set -l branch_name (command git -C $repo_path rev-parse --abbrev-ref HEAD)
                _legit_repo_interact_update $repo_path $branch_name
            case '*'
                _legit_repo_interact_print_help
        end

        printf "\n"
    end
end

###########################################################
## Pull remote changes into local branch.
##
## Remote changes are pulled from remote branch that is called like
## the local branch (`git pull origin <local-branch-name>`).
##
## Arguments:
##   repo_path The path of the repository
##   branch_name The name of the branch to pull from
###########################################################
function _legit_repo_interact_update -a "repo_path" "branch_name"
    printf ":: [Repository Update] Pulling changes from branch %sorigin/%s%s...\n\n" \
            (__legit_set_color path) $branch_name (__legit_set_color)
    set -l result (command git -C $repo_path pull origin $branch_name 2>&1)

    if test $status -eq 0
        printf "\n"
        __legit_print_list -c info --nocount $result
    else
        printf "\n"
        __legit_print_list -c error --nocount $result
    end
end

###########################################################
## Push commits to remote.
##
## Commits are pushed to remote branch that is called like
## the local branch (`git push origin <local-branch-name>`).
##
## Arguments:
##   repo_path The path of the repository
##   branch_name The name of the branch to push to
###########################################################
function _legit_repo_interact_push -a "repo_path" "branch_name"
    printf ":: [Repository Push] Pushing changes to remote branch %sorigin/%s%s...\n\n" \
            (__legit_set_color path) $branch_name (__legit_set_color)
    set -l result (command git -C $repo_path push origin $branch_name 2>&1)

    if test $status -eq 0
        printf "\n"
        __legit_print_list -c info --nocount $result
    else
        printf "\n"
        __legit_print_list -c error --nocount $result
    end
end

###########################################################
## Add files to the stage in order to be committed.
##
## Arguments:
##   repo_path The path of the repository
###########################################################
function _legit_repo_interact_add -a "repo_path"
    read --array --prompt 'printf ":: [Repository Add] Type files to add: "' -l files

    set -l result (command git -C $repo_path add $files 2>&1)

    if test $status -eq 0
        __legit_print_list -c info --nocount $result
    else
        __legit_print_list -c error --nocount $result
    end
end

###########################################################
## Commit staged changes (= git commit).
##
## Arguments:
##   repo_path The path of the repository to commit
##             the changes in
###########################################################
function _legit_repo_interact_commit -a "repo_path"
    command git -C $repo_path commit $message
end

function _legit_repo_interact_print_current_branch -a "repo_path"
    set -l branch_name (command git -C $repo_path branch | sed -n '/\* /s///p')

    printf "=> Current branch: %s%s%s\n\n" \
            (__legit_set_color path) $branch_name (__legit_set_color)
end

function _legit_repo_interact_print_current -a "repo_path"
    printf ":: Switching to repository %s...\n" $repo_path
    printf "=> Current repository: %s%s%s\n" \
            (__legit_set_color path) $repo_path (__legit_set_color)
    _legit_repo_interact_print_current_branch $repo_path
end

function _legit_repo_interact_print_prompt_help
    printf ":: [Repository] Type an action or (%sh%s)elp: " \
           (__legit_set_color prompt) (__legit_set_color)
end

function _legit_repo_interact_print_help
    printf "\n"
    printf "\tOptions currently available:\n"
    printf "\t\t(%sa%s)dd\t\tAdd changes to the stage (= git add <files|directories>)\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%sb%s)ranch\tSwitch into branch mode\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%sc%s)ommit\tCommit changes on current branch (= git commit)\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%se%s)xecute\tExectue an arbitrary command in the current repository directory\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t\t\tNotice: When in this mode, exiting this program via 'ctrl+c' will cause\n"
    printf "\t\t\t\ta directory change to the current repository.\n"
    printf "\t\t(%sh%s)elp\t\tPrint available options to interact with a Git repository\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%sp%s)ush\t\tPush all committed changes to remote branch with same\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t\t\tname as the local branch (= git push origin <local-branch-name>)\n"
    printf "\t\t(%sq%s)quit\t\tQuit interactive repository view\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%ss%s)tatus\tShow a summary of the status of the current branch (=git status)\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%su%s)pdate\tPull changes from remote branch with the same name\n" \
              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t\t\tas the local branch (= git pull origin <local-branch-name>)\n"
    printf "\n"
end
