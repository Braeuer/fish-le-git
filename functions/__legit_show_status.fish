#-------------------------------------------------------------------------------
#
# Description : A fish shell function for showing a minimized
#               version of `git status` in a repository.
#
# Author      : Matthias Bräuer <matthias@braeuer.dev>
# Date        : 2019-26-10
# License     : MIT
#
#-------------------------------------------------------------------------------

function __legit_show_status -a "repo_path"
    set -l git_status (git -C $repo_path status)
    set git_status (string match -r -v \
        '.*(git .+ <file>|On branch|up to date|no changes).*' $git_status)

    for line in $git_status
        printf "\t%s%s%s\n" (__legit_set_color info) $line (__legit_set_color)
    end
end
