#-------------------------------------------------------------------------------
#
# Description : A fish shell function for interacting with a Git branch
#               (e.g. checkout, delete, ...).
#
# Author      : Matthias Bräuer <matthias@braeuer.dev>
# Date        : 2019-10-28
# License     : MIT
#
#-------------------------------------------------------------------------------

function __legit_branch_interact -a "repo_path"
    _legit_branch_interact_print_mode $repo_path

    set -l branches (__legit_get_branches $repo_path)
    _legit_branch_interact_list $branches

    while true
        read -n 1 --prompt "_legit_branch_interact_print_prompt_help" -l action

        switch $action
            case c
                _legit_branch_interact_checkout $repo_path
            case d
                _legit_branch_interact_delete $repo_path $branches
            case h
                _legit_branch_interact_print_help
            case l
                set branches (__legit_get_branches $repo_path)
                _legit_branch_interact_list $branches
            case n
                _legit_branch_interact_new $repo_path
            case q
                return
            case s
                _legit_branch_interact_switch $repo_path $branches
            case '*'
                _legit_branch_interact_print_help
        end
    end
end

#####################################################
## Create a new Git branch and switch to it.
##
## Arguments:
##   repo_path The path of the repository to checkout
##             a branch in
#####################################################
function _legit_branch_interact_checkout -a "repo_path"
    read --prompt 'printf ":: [Branch Checkout] Choose name: "' -l branch
    set -l result (command git -C $repo_path checkout -b $branch 2>&1)

    if test $status -eq 0
        printf "\n\t%sSuccessfully switched to new branch %s.%s\n\n" \
              (__legit_set_color info) $branch (__legit_set_color)
    else
        printf "\n\t%s%s%s\n\n" \
              (__legit_set_color error) $result (__legit_set_color)
    end
end

#####################################################
## Switch to another Git branch.
##
## Arguments:
##   repo_path The path of the repository
##   branches  The branches of the repository
#####################################################
function _legit_branch_interact_switch -a "repo_path"
    set -e argv[1]
    set -l branches $argv
    set -l branches_count (count $branches)
    read --prompt "_legit_branch_interact_print_prompt_checkout $branches_count" \
                  -l branch_num

    # Test whether a number was typed in. If not print error and return.
    if test (__legit_is_number $branch_num) = false
        printf "\n\t%sError: '%s' is not a number%s\n\n" \
            (__legit_set_color error) $branch_num (__legit_set_color)
        return
    end

    # Test whether the typed in number is within the range
    # of existing branches. If not print error and return.
    if test $branch_num -gt $branches_count -o $branch_num -lt 1
        printf "\n\t%sError: Branch number must be between 1 and %d%s\n\n" \
            (__legit_set_color error) $branches_count (__legit_set_color)
        return
    end

    # Switch to branch with number $branch_num
    set -l result (command git -C $repo_path checkout \
          (string trim (string replace '*' '' $branches[$branch_num])) 2>&1)

    # Print result of switching to the branch
    if test $status -eq 0
        printf "\n"
        __legit_print_list -c info -n $result
        printf "\n"
    else
        printf "\n\t%s%s%s\n\n" \
            (__legit_set_color error) $result (__legit_set_color)
    end
end

#####################################################
## Print a list of Git branches.
##
## Arguments:
##   branches The branches of the repository
#####################################################
function _legit_branch_interact_list
    printf "\n"
    __legit_print_list -c info $argv
    printf "\n"
end

#####################################################
## Delete a Git branch.
##
## Arguments:
##   repo_path The path of the repository to delete a
##             branch from
##   branches  The branches of the repository
#####################################################
function _legit_branch_interact_delete -a "repo_path"
    set -e argv[1]
    set -l branches $argv
    set -l branches_count (count $branches)
    read --prompt "_legit_branch_interact_print_prompt_delete $branches_count" \
                  -l branch_num

    # Test whether a number was typed in. If not print error and return.
    if test (__legit_is_number $branch_num) = false
        printf "\n\t%sError: '%s' is not a number%s\n\n" \
            (__legit_set_color error) $branch_num (__legit_set_color)
        return
    end

    # Test whether the typed in number is within the range
    # of existing branches. If not print error and return.
    if test $branch_num -gt $branches_count -o $branch_num -lt 1
        printf "\n\t%sError: Branch number must be between 1 and %d%s\n\n" \
            (__legit_set_color error) $branches_count (__legit_set_color)
        return
    end

    # Delete branch with number $branch_num
    set -l result (command git -C $repo_path branch -d \
          (string trim (string replace '*' '' $branches[$branch_num])) 2>&1)

    # Print result of deleting the branch
    if test $status -eq 0
        printf "\n\t%s%s%s\n\n" \
            (__legit_set_color info) $result (__legit_set_color)
    else
        printf "\n\t%s%s%s\n\n" \
            (__legit_set_color error) $result (__legit_set_color)
    end
end

#####################################################
## Create a new Git branch.
##
## Arguments:
##   repo_path The path of the repository to delete a
##             branch from
#####################################################
function _legit_branch_interact_new -a "repo_path"
    read --prompt 'printf ":: [Branch New] Choose name: "' -l branch
    set -l result (command git -C $repo_path branch $branch 2>&1)

    if test $status -eq 0
        printf "\n\t%sSuccessfully created branch %s.%s\n\n" \
              (__legit_set_color info) $branch (__legit_set_color)
    else
        printf "\n\t%s%s%s\n\n" \
              (__legit_set_color error) $result (__legit_set_color)
    end
end

function _legit_branch_interact_print_mode -a "repo_path"
    printf ":: Switching to branch mode...\n"
    printf ":: Listing all local branches of %s%s%s...\n" \
            (__legit_set_color path) $repo_path (__legit_set_color)
end

function _legit_branch_interact_print_prompt_help
    printf ":: [Branch] Type an action or (%sh%s)elp: " \
            (__legit_set_color prompt) (__legit_set_color)
end

function _legit_branch_interact_print_help
    printf "\n"
    printf "\tOptions currently available:\n"
    printf "\t\t(%sc%s)heckout\tCheckout new branch (= git checkout -b <branch-name>)\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%sd%s)elete\tDelete branch with number <n>\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%sh%s)elp\t\tPrint available options to interact with a Git branch\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%sl%s)ist\t\tList all local branches of the current Git repository\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%sn%s)ew\t\tCreate new Git branch with name <name>\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%sq%s)uit\t\tQuit branch mode\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\t\t(%ss%s)witch\tSwitch to a branch with number <n>\n" \
                              (__legit_set_color prompt) (__legit_set_color)
    printf "\n"
end

function _legit_branch_interact_print_prompt_checkout -a "count"
    printf ":: [Branch Checkout] Choose branch (%s1%s-%s%d%s): " \
            (__legit_set_color prompt) (__legit_set_color) \
            (__legit_set_color prompt) $count (__legit_set_color)
end

function _legit_branch_interact_print_prompt_delete -a "count"
    printf ":: [Branch Delete] Choose branch (%s1%s-%s%d%s): " \
            (__legit_set_color prompt) (__legit_set_color) \
            (__legit_set_color prompt) $count (__legit_set_color)
end
