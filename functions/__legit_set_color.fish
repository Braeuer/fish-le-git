#-------------------------------------------------------------------------------
#
# Description : A fish shell function to set the color of a specific
#               status (info, error, ...) or the normal color.
#
# Author      : Matthias Bräuer <matthias@braeuer.dev>
# Date        : 2019-10-27
# License     : MIT
#
#-------------------------------------------------------------------------------

function __legit_set_color -a "color_status"
    if test -z $color_status
        set color_status normal
    end

    switch $color_status
        case path
             # Olive
             set_color 606000
        case prompt
             # Dark Orange
             set_color FF8C00
        case info
             # Cornflower Blue
             set_color 5080C0
        case error
             # Dark Red
             set_color 8B0000
        case '*'
             set_color normal
    end
end
