#-------------------------------------------------------------------------------
#
# Description : A fish shell function for testing whether a
#               variable is a number or not.
#
# Author      : Matthias Bräuer <matthias@braeuer.dev>
# Date        : 2019-10-29
# License     : MIT
#
#-------------------------------------------------------------------------------

function __legit_is_number
    if string match -qr '^[0-9]+$' $argv
        echo true
    else
        echo false
    end
end
