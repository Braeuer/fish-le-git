#-------------------------------------------------------------------------------
#
# Description : A fish shell function for finding Git repositories
#               within a certain path and execute Git commands for
#               a repository interactively.
#
# Author      : Matthias Bräuer
# Date        : 2019-10-26
# License     : MIT
#
#-------------------------------------------------------------------------------

function legit -d "Find all git repositories and interact with them"
    argparse -n git_find 'd/dir=' -- $argv or return
    set -q _flag_dir; or set -l _flag_dir ~

    set -l repos_path
    set -l repos_count
    set -l search_repos true

    while true
        if $search_repos
            printf ":: Searching Git repositories in %s%s%s... " \
                   (__legit_set_color path) $_flag_dir (__legit_set_color)
            set repos_paths (__legit_repos_find $_flag_dir)
            set repos_count (count $repos_paths)
            printf "%s%d found%s\n\n" \
                   (__legit_set_color info) $repos_count (__legit_set_color)

            __legit_print_list --color path $repos_paths

            set search_repos false
        end

        if test $repos_count -eq 0
            return
        end

        printf "\n"
        read --prompt 'printf ":: Choose repository (%s1%s-%s%d%s) or (%sq%s)uit: " \
                            (__legit_set_color prompt) (__legit_set_color) \
                            (__legit_set_color prompt) $repos_count (__legit_set_color) \
                            (__legit_set_color prompt) (__legit_set_color)' \
                            -l action

        switch $action
            case q
                clear
                return
            case '*'
                set -l repo_num $action

                if test $repo_num -gt $repos_count -o $repo_num -lt 1
                    printf "%sError: Repository number must be between 1 and %d%s\n" \
                      (__legit_set_color error) $repos_count (__legit_set_color)
                else
                    printf "\n"
                    clear
                    __legit_repo_interact $repos_paths[$repo_num]
                    printf "\n\n"
                    set search_repos true
                    clear
                end
        end
    end
end
