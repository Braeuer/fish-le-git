#-------------------------------------------------------------------------------
# Description: A fish shell function for finding all Git repositories
#              within a directory tree.
#
# Author     : Matthias Bräuer <matthias@braeuer.dev>
# Date       : 2019-10-27
# License    : MIT
#
#-------------------------------------------------------------------------------

function __legit_repos_find -a "dir"
    find $dir -name ".*" -prune -name '.git' -type d -printf "%h\n" 2> /dev/null
end
