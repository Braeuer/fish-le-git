#-------------------------------------------------------------------------------
#
# Description : A fish shell function for getting all branches of
#               a git repository.
#
# Author      : Matthias Bräuer <matthias@braeuer.dev>
# Date        : 2019-10-27
# License     : MIT
#
#-------------------------------------------------------------------------------

function __legit_get_branches -a "repo_path"
    git -C $repo_path branch
end
